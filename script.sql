--================================================================
---------------------------- EMPLOYEES --------------------------
drop table if exists EMPLOYEES cascade;
create table EMPLOYEES (
 emp_id          smallint      
,emp_first_name  varchar(20)   not null
,emp_last_name   varchar(20)       null
,emp_hiring_date date          not null
,title_id        smallint      not null    
,title_name      varchar(50)   not null
,dep_id          smallint      not null
,dep_name        varchar(50)   not null
,parent_dep_id   smallint      not null
,parent_dep_name varchar(12)   not null
,emp_salary      numeric(10,2) not null
,emp_status      varchar(20)   not null
,emp_exit_date   date              null
,emp_skills      varchar(255)  not null
,primary key (emp_id)
);

copy EMPLOYEES
from 'D:\Develop\Epam_frontend_online\Stage4_TypeScript\practies\employees.csv'
delimiter '|'
csv header;
--select * from EMPLOYEES;
--================================================================
---------------------------- SKILLS ------------------------------
drop table if exists SKILLS_ATTR cascade;
create table SKILLS_ATTR (
     skills_id      serial
    ,skills_name    varchar(50) not null
    ,primary key (skills_id)
);

insert into SKILLS_ATTR(skills_name)
select distinct 
       trim(t2.emp_skills) as skills_name
  from EMPLOYEES as t1
  cross join unnest(string_to_array(t1.emp_skills, ';')) as t2(emp_skills)
;

drop table if exists EMP_SKILLS cascade;
create table EMP_SKILLS (
     emp_id     smallint  
    ,skills_id  smallint
    ,constraint employees_fk_constr foreign key(emp_id) references EMPLOYEES(emp_id)
    ,constraint slills_fk_constr foreign key(skills_id) references SKILLS_ATTR(skills_id)
);

insert into EMP_SKILLS
select  t1.emp_id
       ,t3.skills_id
  from EMPLOYEES t1
  cross join unnest(string_to_array(t1.emp_skills, ';')) as t2(emp_skills)
  join SKILLS_ATTR     as t3 on t3.skills_name = trim(t2.emp_skills)
;

alter table EMPLOYEES drop column emp_skills;

--================================================================
----------------------------- STATUS ------------------------------
drop table if exists STATUS_ATTR cascade;
create table STATUS_ATTR (
     status_id   serial
    ,status_name varchar(20) not null
    ,primary key (status_id)
);

insert into STATUS_ATTR(status_name)
select distinct emp_status from EMPLOYEES;

drop table if exists EMP_STATUS cascade;
create table EMP_STATUS (
     emp_id         smallint  
    ,status_id      smallint
    ,constraint employees_fk_constr foreign key(emp_id) references EMPLOYEES(emp_id)
    ,constraint status_fk_constr foreign key(status_id) references STATUS_ATTR(status_id)
);

insert into EMP_STATUS
select  t1.emp_id
       ,t2.status_id
  from EMPLOYEES      as t1
  join STATUS_ATTR    as t2 on t2.status_name = t1.emp_status
;

alter table EMPLOYEES drop column emp_status;

--================================================================
-------------------------- TITLE ---------------------------------
drop table if exists TITLE_ATTR cascade;
create table TITLE_ATTR (
     title_id  smallint
    ,title_name    varchar(50) not null
    ,primary key (title_id)
);

insert into TITLE_ATTR
select distinct
        title_id
       ,title_name
  from EMPLOYEES
 order by title_id
;

alter table EMPLOYEES drop column title_name;

drop table if exists EMP_TITLE cascade;
create table EMP_TITLE (
     emp_id    smallint  
    ,title_id  smallint
    ,constraint employees_fk_constr foreign key(emp_id) references EMPLOYEES(emp_id)
    ,constraint title_fk_constr foreign key(title_id) references TITLE_ATTR(title_id)
);

insert into EMP_TITLE
select  t1.emp_id
       ,t2.title_id
  from EMPLOYEES  as t1
  join TITLE_ATTR as t2 on t2.title_id = t1.title_id
;

alter table EMPLOYEES drop column title_id;

--================================================================
------------------------------ DEP -------------------------------
drop table if exists DEP_ATTR cascade;
create table DEP_ATTR (
     dep_id  smallint        null
    ,dep_name    varchar(50) null
    ,parent_dep_id smallint
    ,primary key (dep_id)
    ,constraint parent_dep_fk_constr foreign key(parent_dep_id) references DEP_ATTR(dep_id)
);

insert into DEP_ATTR
select distinct
        t1.dep_id
       ,t1.dep_name
       ,t2.parent_dep_id
  from (
	  select dep_id, dep_name   from EMPLOYEES
	union
 	  select parent_dep_id, parent_dep_name from EMPLOYEES
  					) as t1
  left join EMPLOYEES as t2 on t2.dep_id = t1.dep_id
 order by t1.dep_id
;

alter table EMPLOYEES add constraint dep_attr_fk_constr foreign key(dep_id) references DEP_ATTR(dep_id);

alter table EMPLOYEES 
      drop column dep_name,
      drop column parent_dep_id,
      drop column parent_dep_name
;

--==================================================================
------------------------------ PROCEDURE ---------------------------
--drop procedure PRINT_INFO_BY_DEP;
create or replace procedure PRINT_INFO_BY_DEP(depid in int) 
as $$
declare
	e_id         EMPLOYEES.emp_id%type;
	e_full_name  varchar(30);
	e_title      TITLE_ATTR.title_name%type;
	e_salary     EMPLOYEES.emp_salary%type;
	e_hire_date  EMPLOYEES.emp_hiring_date%type;
	e_count      int;
	e_avg_salary int;
	
	CURS1 cursor for 
	select t2.emp_id 
	      ,trim(coalesce(t2.emp_first_name, '')||' '||coalesce(t2.emp_last_name,''))
		  ,t4.title_name 
		  ,t2.emp_salary
		  ,t2.emp_hiring_date 
	 from DEP_ATTR    as t1
	 join EMPLOYEES   as t2 on t2.dep_id = t1.dep_id
	 join EMP_TITLE   as t3 on t3.emp_id = t2.emp_id
	 join TITLE_ATTR  as t4 on t4.title_id = t3.title_id
	 join EMP_STATUS  as t6 on t6.emp_id = t2.emp_id
	 join STATUS_ATTR as t7 on t7.status_id = t6.status_id
	                       and t7.status_name = 'active'
	where t1.dep_id = depid or t1.parent_dep_id = depid
	;
	
	CURS2 cursor for 
	select  count(t2.emp_id) 
	       ,avg(t2.emp_salary)
	 from DEP_ATTR    as t1
	 join EMPLOYEES   as t2 on t2.dep_id = t1.dep_id
	 join EMP_TITLE   as t3 on t3.emp_id = t2.emp_id
	 join TITLE_ATTR  as t4 on t4.title_id = t3.title_id
	 join EMP_STATUS  as t6 on t6.emp_id = t2.emp_id
	 join STATUS_ATTR as t7 on t7.status_id = t6.status_id
	                       and t7.status_name = 'active'
	where t1.dep_id = depid or t1.parent_dep_id = depid
	;
begin

    open CURS1;
	loop
		fetch CURS1 into e_id, e_full_name, e_title, e_salary, e_hire_date;
		if not found then 
			exit;
		end if;
		raise notice '%, %, %, %, %', e_id, e_full_name, e_title, e_salary, age(e_hire_date);
	end loop;
	close CURS1; 
	
	open CURS2;
	loop 
		fetch CURS2 into e_count, e_avg_salary;
		if not found then
			exit;
		end if;
		raise notice 'Count of employees and average salary: %, %', e_count, e_avg_salary;
	end loop;
	close CURS2;

end;
$$
language 'plpgsql';

call PRINT_INFO_BY_DEP(6);
call PRINT_INFO_BY_DEP(5);
call PRINT_INFO_BY_DEP(2);
call PRINT_INFO_BY_DEP(1);