# hw-sql

INSTALATION

Go to website https://www.enterprisedb.com/downloads/postgres-postgresql-downloads and download PostgreSQL Database for Windows x86-64, version - 13.2., then follow the prompts to install the app

DATABASE CLIENT

Using the standard client, which is supplied in the installation package (PgAdmin 4)

CONECTION TO DB

To connect you need enter:
- Database UserId: postgres
- Database Password: alex123

Then you have to create databese with name copr

RUN SCRIPT

- Download employees.csv and result.sql from gitlab: https://gitlab.com/oleksandr_romanko/hw-sql
- Open the result.sql file using PgAdmin and edit the path to load the source file into the database, for example
```
copy EMPLOYEES
from 'your_full_path_to_the_file\employees.csv'
delimiter '|'
csv header;
```
- Just run script!!!
