PGDMP     '    
                y           corp    13.2    13.2 *    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16394    corp    DATABASE     d   CREATE DATABASE "corp" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Russian_Ukraine.1251';
    DROP DATABASE "corp";
                postgres    false            �            1255    18589    print_info_by_dep(integer) 	   PROCEDURE     _  CREATE PROCEDURE "public"."print_info_by_dep"("depid" integer)
    LANGUAGE "plpgsql"
    AS '
declare
	e_id         EMPLOYEES.emp_id%type;
	e_full_name  varchar(30);
	e_title      TITLE_ATTR.title_name%type;
	e_salary     EMPLOYEES.emp_salary%type;
	e_hire_date  EMPLOYEES.emp_hiring_date%type;
	e_count      int;
	e_avg_salary int;
	
	CURS1 cursor for 
	select t1.emp_id 
	      ,trim(coalesce(t1.emp_first_name, '''')||'' ''||coalesce(t1.emp_last_name,''''))
		  ,t3.title_name 
		  ,t1.emp_salary
		  ,t1.emp_hiring_date 
	 from EMPLOYEES   as t1
	 join EMP_TITLE   as t2 on t2.emp_id = t1.emp_id
	 join TITLE_ATTR  as t3 on t3.title_id = t2.title_id
	 join DEP_TREE    as t4 on t4.descendant = t1.dep_id
                               and t4.ancestor = depid
	 join EMP_STATUS  as t6 on t6.emp_id = t1.emp_id
	 join STATUS_ATTR as t7 on t7.status_id = t6.status_id
	                       and t7.status_name = ''active''
	;
	
	CURS2 cursor for 
	select  count(t1.emp_id) 
	       ,avg(t1.emp_salary)
	  from EMPLOYEES  as t1
	  join EMP_TITLE  as t2 on t2.emp_id = t1.emp_id
	  join TITLE_ATTR as t3 on t3.title_id = t2.title_id
	  join DEP_TREE   as t4 on t4.descendant = t1.dep_id
                               and t4.ancestor = depid
	 join EMP_STATUS  as t6 on t6.emp_id = t1.emp_id
	 join STATUS_ATTR as t7 on t7.status_id = t6.status_id
	                       and t7.status_name = ''active''
	;
begin

    open CURS1;
	loop
		fetch CURS1 into e_id, e_full_name, e_title, e_salary, e_hire_date;
		if not found then 
			exit;
		end if;
		raise notice ''%, %, %, %, %'', e_id, e_full_name, e_title, e_salary, age(e_hire_date);
	end loop;
	close CURS1; 
	
	open CURS2;
	loop 
		fetch CURS2 into e_count, e_avg_salary;
		if not found then
			exit;
		end if;
		raise notice ''Count of employees and average salary: %, %'', e_count, e_avg_salary;
	end loop;
	close CURS2;

end;
';
 >   DROP PROCEDURE "public"."print_info_by_dep"("depid" integer);
       public          postgres    false            �            1259    19591    dep_attr    TABLE     �   CREATE TABLE "public"."dep_attr" (
    "dep_id" smallint NOT NULL,
    "dep_name" character varying(50),
    "parent_dep_id" smallint
);
     DROP TABLE "public"."dep_attr";
       public         heap    postgres    false            �            1259    19539 
   emp_skills    TABLE     X   CREATE TABLE "public"."emp_skills" (
    "emp_id" smallint,
    "skills_id" smallint
);
 "   DROP TABLE "public"."emp_skills";
       public         heap    postgres    false            �            1259    19560 
   emp_status    TABLE     X   CREATE TABLE "public"."emp_status" (
    "emp_id" smallint,
    "status_id" smallint
);
 "   DROP TABLE "public"."emp_status";
       public         heap    postgres    false            �            1259    19578 	   emp_title    TABLE     V   CREATE TABLE "public"."emp_title" (
    "emp_id" smallint,
    "title_id" smallint
);
 !   DROP TABLE "public"."emp_title";
       public         heap    postgres    false            �            1259    19526 	   employees    TABLE     2  CREATE TABLE "public"."employees" (
    "emp_id" smallint NOT NULL,
    "emp_first_name" character varying(20) NOT NULL,
    "emp_last_name" character varying(20),
    "emp_hiring_date" "date" NOT NULL,
    "dep_id" smallint NOT NULL,
    "emp_salary" numeric(10,2) NOT NULL,
    "emp_exit_date" "date"
);
 !   DROP TABLE "public"."employees";
       public         heap    postgres    false            �            1259    19533    skills_attr    TABLE     |   CREATE TABLE "public"."skills_attr" (
    "skills_id" integer NOT NULL,
    "skills_name" character varying(50) NOT NULL
);
 #   DROP TABLE "public"."skills_attr";
       public         heap    postgres    false            �            1259    19531    skills_attr_skills_id_seq    SEQUENCE     �   CREATE SEQUENCE "public"."skills_attr_skills_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE "public"."skills_attr_skills_id_seq";
       public          postgres    false    206            �           0    0    skills_attr_skills_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE "public"."skills_attr_skills_id_seq" OWNED BY "public"."skills_attr"."skills_id";
          public          postgres    false    205            �            1259    19554    status_attr    TABLE     |   CREATE TABLE "public"."status_attr" (
    "status_id" integer NOT NULL,
    "status_name" character varying(20) NOT NULL
);
 #   DROP TABLE "public"."status_attr";
       public         heap    postgres    false            �            1259    19552    status_attr_status_id_seq    SEQUENCE     �   CREATE SEQUENCE "public"."status_attr_status_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE "public"."status_attr_status_id_seq";
       public          postgres    false    209            �           0    0    status_attr_status_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE "public"."status_attr_status_id_seq" OWNED BY "public"."status_attr"."status_id";
          public          postgres    false    208            �            1259    19573 
   title_attr    TABLE     z   CREATE TABLE "public"."title_attr" (
    "title_id" smallint NOT NULL,
    "title_name" character varying(50) NOT NULL
);
 "   DROP TABLE "public"."title_attr";
       public         heap    postgres    false            E           2604    19536    skills_attr skills_id    DEFAULT     �   ALTER TABLE ONLY "public"."skills_attr" ALTER COLUMN "skills_id" SET DEFAULT "nextval"('"public"."skills_attr_skills_id_seq"'::"regclass");
 J   ALTER TABLE "public"."skills_attr" ALTER COLUMN "skills_id" DROP DEFAULT;
       public          postgres    false    205    206    206            F           2604    19557    status_attr status_id    DEFAULT     �   ALTER TABLE ONLY "public"."status_attr" ALTER COLUMN "status_id" SET DEFAULT "nextval"('"public"."status_attr_status_id_seq"'::"regclass");
 J   ALTER TABLE "public"."status_attr" ALTER COLUMN "status_id" DROP DEFAULT;
       public          postgres    false    209    208    209            �          0    19591    dep_attr 
   TABLE DATA                 public          postgres    false    213   6       �          0    19539 
   emp_skills 
   TABLE DATA                 public          postgres    false    207   �6       �          0    19560 
   emp_status 
   TABLE DATA                 public          postgres    false    210   �7       �          0    19578 	   emp_title 
   TABLE DATA                 public          postgres    false    212   8       �          0    19526 	   employees 
   TABLE DATA                 public          postgres    false    204   :9       �          0    19533    skills_attr 
   TABLE DATA                 public          postgres    false    206   �;       �          0    19554    status_attr 
   TABLE DATA                 public          postgres    false    209   �<       �          0    19573 
   title_attr 
   TABLE DATA                 public          postgres    false    211   9=       �           0    0    skills_attr_skills_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('"public"."skills_attr_skills_id_seq"', 16, true);
          public          postgres    false    205            �           0    0    status_attr_status_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('"public"."status_attr_status_id_seq"', 3, true);
          public          postgres    false    208            P           2606    19595    dep_attr dep_attr_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY "public"."dep_attr"
    ADD CONSTRAINT "dep_attr_pkey" PRIMARY KEY ("dep_id");
 F   ALTER TABLE ONLY "public"."dep_attr" DROP CONSTRAINT "dep_attr_pkey";
       public            postgres    false    213            H           2606    19530    employees employees_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY "public"."employees"
    ADD CONSTRAINT "employees_pkey" PRIMARY KEY ("emp_id");
 H   ALTER TABLE ONLY "public"."employees" DROP CONSTRAINT "employees_pkey";
       public            postgres    false    204            J           2606    19538    skills_attr skills_attr_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY "public"."skills_attr"
    ADD CONSTRAINT "skills_attr_pkey" PRIMARY KEY ("skills_id");
 L   ALTER TABLE ONLY "public"."skills_attr" DROP CONSTRAINT "skills_attr_pkey";
       public            postgres    false    206            L           2606    19559    status_attr status_attr_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY "public"."status_attr"
    ADD CONSTRAINT "status_attr_pkey" PRIMARY KEY ("status_id");
 L   ALTER TABLE ONLY "public"."status_attr" DROP CONSTRAINT "status_attr_pkey";
       public            postgres    false    209            N           2606    19577    title_attr title_attr_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY "public"."title_attr"
    ADD CONSTRAINT "title_attr_pkey" PRIMARY KEY ("title_id");
 J   ALTER TABLE ONLY "public"."title_attr" DROP CONSTRAINT "title_attr_pkey";
       public            postgres    false    211            Q           2606    19601    employees dep_attr_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY "public"."employees"
    ADD CONSTRAINT "dep_attr_fk_constr" FOREIGN KEY ("dep_id") REFERENCES "public"."dep_attr"("dep_id");
 L   ALTER TABLE ONLY "public"."employees" DROP CONSTRAINT "dep_attr_fk_constr";
       public          postgres    false    2896    213    204            R           2606    19542    emp_skills employees_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY "public"."emp_skills"
    ADD CONSTRAINT "employees_fk_constr" FOREIGN KEY ("emp_id") REFERENCES "public"."employees"("emp_id");
 N   ALTER TABLE ONLY "public"."emp_skills" DROP CONSTRAINT "employees_fk_constr";
       public          postgres    false    2888    204    207            T           2606    19563    emp_status employees_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY "public"."emp_status"
    ADD CONSTRAINT "employees_fk_constr" FOREIGN KEY ("emp_id") REFERENCES "public"."employees"("emp_id");
 N   ALTER TABLE ONLY "public"."emp_status" DROP CONSTRAINT "employees_fk_constr";
       public          postgres    false    204    2888    210            V           2606    19581    emp_title employees_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY "public"."emp_title"
    ADD CONSTRAINT "employees_fk_constr" FOREIGN KEY ("emp_id") REFERENCES "public"."employees"("emp_id");
 M   ALTER TABLE ONLY "public"."emp_title" DROP CONSTRAINT "employees_fk_constr";
       public          postgres    false    2888    212    204            X           2606    19596    dep_attr parent_dep_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY "public"."dep_attr"
    ADD CONSTRAINT "parent_dep_fk_constr" FOREIGN KEY ("parent_dep_id") REFERENCES "public"."dep_attr"("dep_id");
 M   ALTER TABLE ONLY "public"."dep_attr" DROP CONSTRAINT "parent_dep_fk_constr";
       public          postgres    false    213    213    2896            S           2606    19547    emp_skills slills_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY "public"."emp_skills"
    ADD CONSTRAINT "slills_fk_constr" FOREIGN KEY ("skills_id") REFERENCES "public"."skills_attr"("skills_id");
 K   ALTER TABLE ONLY "public"."emp_skills" DROP CONSTRAINT "slills_fk_constr";
       public          postgres    false    2890    206    207            U           2606    19568    emp_status status_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY "public"."emp_status"
    ADD CONSTRAINT "status_fk_constr" FOREIGN KEY ("status_id") REFERENCES "public"."status_attr"("status_id");
 K   ALTER TABLE ONLY "public"."emp_status" DROP CONSTRAINT "status_fk_constr";
       public          postgres    false    2892    210    209            W           2606    19586    emp_title title_fk_constr    FK CONSTRAINT     �   ALTER TABLE ONLY "public"."emp_title"
    ADD CONSTRAINT "title_fk_constr" FOREIGN KEY ("title_id") REFERENCES "public"."title_attr"("title_id");
 I   ALTER TABLE ONLY "public"."emp_title" DROP CONSTRAINT "title_fk_constr";
       public          postgres    false    211    2894    212            �   �   x���M
�0�ὧ���`]�*X�ݖ4k��c��(=B�{a��I�*)kH� ��g'���A����'!,%i�s+�Q��︆{�ݒ
�(�����4��B~˲��K�m-�^H1M���"�n�&i�|9B��R�F���i�h�h�K�j��8~�To�-yp@�,Y�qU#k��}�G�      �   �   x��ֿ
�0��ݧ82)H1�h�S�X��k��A��=�G�wL�\�Kڮo����p3���x3�x͗�9N�b$��ƻI����E"�������,�F�ߒ-1*���� OA���������MÔ#RMAk���T*1���aR�I&ٌ�<Gi6v�+��t�2�����*
Z# �OIz�
����I9&i,��;���q���r�(MzE}2H3US����E^ˤ]      �   �   x���v
Q���WP*(M��LV�SJ�-�/.I,)-VR� �2S�t� b ��B��O�k���������5�'�&YP�$s��dF5�L�f�	�L2��I�2�jɉj��j��ji�jI�j)Ɉj	�j�dD���e�!�$c�I\\ &ɝ9      �   �   x��ӱ
�0��ݧ82)�Sc"�:8��j��:,8����W��2���a��Da�.d��s�/s0�g}lq[fC�>ķa2�Wzt?�o�H�0Iq�L�T���0S`,�0-�q�c�N�)��L-�ѐ�ђ=�є;��-�B�bnK4gAܺ8Ц=h�)鴡,�	���      �   P  x�՗ˎ�0��<E�&3ٹ����ʌ�]"��`���IڦO�sl����I�\t>����̖���ڙ-��N�ؤ������8���;8؈�?p��O��zS���gr�nb/�(w���z*��W,g�=��Q������t�<Ё�~��҅��/|BF�<���8���D��Y�-�z��@��G�b�k�e�>rDghF��J����P��{��(�E�[��H����&��~V|'U�(_dS�;� F��D�_�E�7�v^e����X��`BHb�{)�2G���
V�����I��럨�/��D���iQ0ݍkYށ���LB�I�AF�{�2�KmW�4���Sb|��_�Y~��ĔX��(��s*~��y)Sq�	��&�\��0����f������kC�[�	��Q��^XVh����+�P��@6��u����14�] �}���^�z-�M��;ͤ���l�A��S��N^�T_� A7�c7���vC1|b�����"�����d�؎\F1,Y�r���T�5��	=?:��Хuhr��,0ᬺ�0�����}���X��      �     x����jA�O梂�n�~�S��B��J��v��b2��o�Y�����7!ɿ^oW�;�׻�����Fqk��=�L.�i�.�GGj
�/o�-L��up�˔������Q-�����Fc6�K��R��,�.��(���?I����p��0?�������
�?�K��8��$������R����T�l�C�'r�$_����>�j��xf5$��1c"h�'��\��}�����gb��/�nQV�j	J�y4�����      �   q   x���v
Q���WP*(M��LV�S*.I,)-�O,))RRЀq3S�t`����T%M�0G�P�`C���̲TuMk.O*�j453�����M,I-��,�T�IM��� �tU5      �   �   x���MK�@�{~Ű��P���)H��m�D���cH&ag����j���{��evf�u�?TP���a��R�NI[|����t�����̾C7����1/a��ar�o&�������W�y'�[�y�ѻ���Fo��7����g5��G��_���17�}G�]� d��A��,d��6,�r��3�k���39=��;���O3�B�&���ǽ��߱逦q?�C��+�,o4HLH�Oopb     